// v 1.0
// Slugify Plugin
// Usage Example:
// <%= plugins.slugify('Test Here !') %>
// Will return: test-here
flashCore.prototype.slugify = function(text) {
    return text.toString().toLowerCase().trim()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/&/g, '-and-')         // Replace & with 'and'
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-');        // Replace multiple - with single -
};
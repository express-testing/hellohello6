// Setting the animated dot for the categories tabs
// like in text_tabs block
var categoriesDots = function() {
	var tabs = event.target.parentElement;

	if(tabs.classList.contains('categories--dot') > -1) {
		var active_child = tabs.querySelector('.active');
		var dot = tabs.querySelector('.categories__dot');
		if(dot) {
			dot.style.left = (active_child.offsetLeft + Math.floor(active_child.offsetWidth / 2)) + 'px';
		}		
	}	
}

flash.ready(function(){
	flash.listen('.categories__items', 'flashTabsUpdated', function(event){
		var tabs = event.target.parentElement;
		
		if(tabs.classList.contains('categories--dot') > -1) {
			var active_child = tabs.querySelector('.active');
			var dot = tabs.querySelector('.categories__dot');
			if(dot) {
				dot.style.left = (active_child.offsetLeft + Math.floor(active_child.offsetWidth / 2)) + 'px';
			}		
		}
	});

	document.querySelectorAll('.categories--dot').forEach(function(element){
		// Adding the dot
		element.insertAdjacentHTML('beforeend', '<span class="categories__dot"></span>');

		// Triggering the update
		var flashReadyEvent = new Event('flashTabsUpdated');
		element.querySelector('.categories__items').dispatchEvent(flashReadyEvent);
	});
}, 'categories');